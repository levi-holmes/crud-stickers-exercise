var express = require('express');
var router = express.Router();
var server = express()
var queries = require ('../db/queries')

module.exports = router;

router.get('/', (request,response) => {
  queries.getAll()
    .then(stickers => {
      res.json(stickers)
    })
})

router.get('/:id', (request,response) => {
  console.log("Is this working???");
  let id = req.params.id;
  queries.getOne(id)
    .then(sticker => {
      response.json(sticker)
    })
})

router.post('/', (request,response) => {
  let newItem = request.body;
  queries.create(newItem)
    .then(sticker => {
      response.json(sticker[0])
    })

})

router.put('/:id', (request,response) => {
  let id = request.params.id;

  queries.update(id,request.body)
    .returning('*')
    .then(sticker => {
      response.json(sticker[0])
    })
})

router.delete('/:id', (request,response) => {
  let id = request.params.id;

  queries.delete(id)
    .then(sticker => {
      response.json({deleted: true})
    })
	})
