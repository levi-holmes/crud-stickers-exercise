const knex = require('./knex');

function Shows() {
  return knex('sticker');
}

module.exports = {
  getAll() {
    return Shows();
  },
  getOne(id) {
    return Shows().where('id', parseInt(id)).first();
  },
  create(sticker) {
    return Shows().insert(sticker,'*');
  },
  update(id, sticker) {
    return Shows().where('id',id).update(sticker);
  },
  delete(id) {
    return Shows().where('id',id).delete();
  }
}
